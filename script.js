

document.getElementById('findIpButton').addEventListener('click', findLocationByIP);

async function findLocationByIP() {
    try {
        const ipResponse = await fetch('https://api.ipify.org/?format=json');
        const ipData = await ipResponse.json();
        const ip = ipData.ip;

        const locationResponse = await fetch(`http://ip-api.com/json/${ip}`);
        const locationData = await locationResponse.json();

        displayLocation(locationData);
    } catch (error) {
        console.error('Помилка при отриманні даних:', error);
    }
}

function displayLocation(locationData) {
    const resultDiv = document.getElementById('result');
    resultDiv.innerHTML = `
        <p><strong>Континент:</strong> ${locationData.continent}</p>
        <p><strong>Країна:</strong> ${locationData.country}</p>
        <p><strong>Регіон:</strong> ${locationData.regionName}</p>
        <p><strong>Місто:</strong> ${locationData.city}</p>
        <p><strong>Район:</strong> ${locationData.district || 'Н/Д'}</p>
    `;
}



